# Alan
A simple Enigma-like string encryption algorithm. Made with a bit over 30 lines of code.

## Example
```
# Encrypt
$ alan mySecureKey "Another one bites the dust"
½ä×Í¸ÈìÑ´{±Þ×ÊÆv×åÈo¿Äè×

# Decrypt
$ alan -d mySecureKey ½ä×Í¸ÈìÑ´{±Þ×ÊÆv×åÈo¿Äè×
Another one bites the dust
```

But you can also `cat` files:

```
$ alan mySecureKey "Another one bites the dust" > secret.txt

$ alan -d mySecureKey $(cat secret.txt)
Another one bites the dust
```
